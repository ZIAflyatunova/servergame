package tests;


import org.junit.*;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class GetNewWordTest {
    private String word = "ОРАНЖЕРЕЯ";
    private List<String> words = Arrays.asList("АББРЕВИАТУРА", "АБОРИГЕН", "ИДИЛЛИЯ", "ЗЛАТОТКАННЫЙ", "КОЛЛОКВИУМ", "ЯСТВО",
            "ЮННАТ", "ЮНЫЙ", "ЗАПАНИБРАТА", "ЕВАНГЕЛИЕ", "ЦЕЛЛОФАН", "ДИССОНАНС", "ХУДО-БЕДНО", "ГУТТАПЕРЧА", "САВАННА", "ПРОПАГАНДА",
            "ПОДОБРУ-ПОЗДОРОВУ", "ПО-ИНОМУ", "ОРАНЖЕРЕЯ", "ОККУПАЦИЯ", "ВЕЛИКОРОСС", "ВА-БАНК", "БЕЛИБЕРДА", "МОЦИОН", "МЕТАЛЛОЛОМ");

    @Test
    public void methodShouldGetNewWord() {
        Random random = new Random();
        int i = random.nextInt(3) + 0;
        word = words.get(i);

        Assert.assertNotEquals(words.get(i), null);
    }

    public static void main(String[] args) {
        try {
            GetNewWordTest.class.getMethod("methodShouldGetNewWord");
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

}
