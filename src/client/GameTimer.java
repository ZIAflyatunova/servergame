package client;

import java.util.Timer;
import java.util.TimerTask;

public class GameTimer {
    public static boolean finish;
    Timer timer;

    public GameTimer(int seconds) {
        timer = new Timer();
        timer.schedule(new RemindTask(), seconds * 1000);
    }

    public class RemindTask extends TimerTask {
        public void run() {
            System.out.println("Time's up!");
            finish = true;
            timer.cancel();
        }
    }
}
