package client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class MonoThreadClientHandler implements Runnable {
    private Socket clientDialog;
    public static int wordsCount;
    public String word = "ОРАНЖЕРЕЯ";
    public String message;
    public static int points = 0;
    public List<String> words = Arrays.asList("АББРЕВИАТУРА", "АБОРИГЕН", "ИДИЛЛИЯ", "ЗЛАТОТКАННЫЙ", "КОЛЛОКВИУМ", "ЯСТВО",
            "ЮННАТ", "ЮНЫЙ", "ЗАПАНИБРАТА", "ЕВАНГЕЛИЕ", "ЦЕЛЛОФАН", "ДИССОНАНС", "ХУДО-БЕДНО", "ГУТТАПЕРЧА", "САВАННА", "ПРОПАГАНДА",
            "ПОДОБРУ-ПОЗДОРОВУ", "ПО-ИНОМУ", "ОРАНЖЕРЕЯ", "ОККУПАЦИЯ", "ВЕЛИКОРОСС", "ВА-БАНК", "БЕЛИБЕРДА", "МОЦИОН", "МЕТАЛЛОЛОМ");


    public MonoThreadClientHandler(Socket client) {
        this.clientDialog = client;
    }

    @Override
    public void run() {
        try {
            wordsCount = 0;
            // Инициируем каналы общения в сокете, для сервера

            // Канал записи в сокет
            DataOutputStream out = new DataOutputStream(clientDialog.getOutputStream());

            // Канал чтения из сокета
            DataInputStream in = new DataInputStream(clientDialog.getInputStream());
            System.out.println("DataInputStream created");

            System.out.println("DataOutputStream  created");

            // Начинаем диалог с подключенным клиентом в цикле, пока сокет не закрыт клиентом
            while (!clientDialog.isClosed()) {
                System.out.println("server reading from channel");

                // Серверная нить ждёт в канале чтения (inputstream) получения данных клиента после получения данных считывает их
                String entry = in.readUTF();

                if (entry.equals(word)) {
                    points += word.length();
                    getNewWord();
                    message = "Верно. Введите слово " + word;
                    wordsCount++;
                }
                else {
                    message = "Ошибка. Повторите ввод слова " + word;
                }

                out.writeUTF(message);

                // и выводит в консоль
                System.out.println("READ from clientDialog message - " + entry);

                System.out.println("server try writing to channel");
                out.writeUTF(message);
                System.out.println("server Wrote message to clientDialog.");

                // Освобождаем буфер сетевых сообщений
                out.flush();

                // Возвращаемся в началло для считывания нового сообщения
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getNewWord() {
        Random random = new Random();
        int i = random.nextInt(words.size()) + 0;
        word = words.get(i);
    }
}