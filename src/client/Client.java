package client;

import sample.GameField;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;

public class Client implements Runnable {
    static Socket socket;
    final int PORT = 3345;
    final String HOST = "localhost";
    public static DataOutputStream out;
    public static DataInputStream in;


    public Client() {
        try {
            // Создаём сокет общения на стороне клиента
            socket = new Socket(HOST, PORT);
            System.out.println("client connected to socket");
            out = new DataOutputStream(socket.getOutputStream());
            in = new DataInputStream(socket.getInputStream());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        try  {
            System.out.println("client out & in initialized");

            // Создаём рабочий цикл
            while (true) {

                GameField.go();

                // Проталкиваем сообщение из буфера сообщений в канал
                out.flush();


                System.out.println("client wrote & start waiting for data from server...");

                // Забираем ответ из канала сервера в сокте клиента и сохраняем её в in переменную, печатаем в консоль
                System.out.println("reading...");
                String inn = in.readUTF();
                System.out.println(inn);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
