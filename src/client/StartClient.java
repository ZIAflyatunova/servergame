package client;

public class StartClient {
    private static final int PORT = 6501;
    private static final String HOST = "localhost";

    public static void main(String[] args) {
        new Thread(new Client()).start();
    }
}
