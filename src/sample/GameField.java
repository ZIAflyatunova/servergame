package sample;

import client.Client;
import client.GameTimer;
import client.MonoThreadClientHandler;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.scene.control.TextField;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;


import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;


public class GameField extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{



        primaryStage.setTitle("Игра слов");
        GridPane root = new GridPane();
        root.setPadding(new Insets(10,20,30,20));
        //горизонтальные отступы между строками
        root.setHgap(5);
        //вертикальные отступы между столбцами
        root.setVgap(5);

        GridPane rootEnd = new GridPane();
        rootEnd.setPadding( new Insets(90,90,70,90));
        rootEnd.setHgap(10);
        rootEnd.setVgap(10);
        rootEnd.setStyle("-fx-background-color: antiquewhite");
        Scene scene1 = new Scene(rootEnd, 625, 260);
        //primaryStage.setScene(scene1);
        //primaryStage.show();

        Label resultsLabel = new Label("Поздравляем, вы ввели ");
        GridPane.setConstraints(resultsLabel, 0, 2);
        rootEnd.getChildren().addAll(resultsLabel);
        resultsLabel.setFont(Font.font("Sans", 19.0));
        resultsLabel.setStyle("-fx-text-fill: darkslategrey");

        Label result = new Label("" + MonoThreadClientHandler.wordsCount);
        GridPane.setConstraints(result, 1, 2);
        rootEnd.getChildren().addAll(result);
        result.setFont(Font.font("Sans", 19.0));
        result.setStyle("-fx-text-fill: darkslategrey");

        Label word = new Label(" слов за 1 минуту!");
        GridPane.setConstraints(word, 2, 2);
        rootEnd.getChildren().addAll(word);
        word.setFont(Font.font("Sans", 19.0));
        word.setStyle("-fx-text-fill: darkslategrey");

        if (MonoThreadClientHandler.wordsCount == 1) {
            word.setText(" слово за 1 минуту!");
        }
        else {
            if ((MonoThreadClientHandler.wordsCount == 2) || (MonoThreadClientHandler.wordsCount == 3) || (MonoThreadClientHandler.wordsCount == 4)) {
                word.setText(" слова за 1 минуту!");
            }
        }



        //Создание стартового окна
        GridPane rootStart = new GridPane();
        rootStart.setPadding(new Insets(30,30,30,30));
        rootStart.setVgap(10);
        rootStart.setHgap(10);
        Scene scene0 = new Scene(rootStart, 625, 260);
        primaryStage.setScene(scene0);
        primaryStage.show();


        Label description1 = new Label("               Добро пожаловать в игру слов! Правила игры очень просты.");
        GridPane.setConstraints(description1, 2, 1);
        rootStart.getChildren().addAll(description1);


        Label description2 = new Label("Вам нужно написать как можно больше предложенных слов за одну минуту.");
        GridPane.setConstraints(description2, 2, 2);
        rootStart.getChildren().addAll(description2);
        description2.setPadding(new Insets(10,5,60,8));


        Button play = new Button("Play!");
        GridPane.setConstraints(play, 2,3);
        rootStart.getChildren().addAll(play);
        play.setMinWidth(170);
        play.setPadding(new Insets(5,240,5,240));

        TextField name = new TextField("Введите слово...");
        GridPane.setConstraints(name,0, 2);
        root.getChildren().add(name);
        name.setFont(Font.font("Sans", 15.0));
        name.setStyle("-fx-text-fill: darkslategrey");

        Button clear = new Button("Очистить поле");
        GridPane.setConstraints(clear, 3, 2);
        root.getChildren().add(clear);

        Label playerName = new Label("Игра идет...");
        GridPane.setConstraints(playerName, 0,0);
        root.getChildren().add(playerName);

        playerName.setFont(Font.font("Sans", 18.0));
        playerName.setPadding(new Insets(30,0,20,0));

        //для выдачи задания Сервера
        Label serverTask = new Label();
        GridPane.setConstraints(serverTask, 0,1);
        root.getChildren().addAll(serverTask);
        //стили для выдачи задания Сервера
        serverTask.setText("Введите слово ОРАНЖЕРЕЯ");
        serverTask.setFont(Font.font("Sans", 14.0));
        serverTask.setPadding(new Insets(5,0,5,0));
        serverTask.setStyle("-fx-text-fill: darkslategrey");
        serverTask.setMaxWidth(450.0);

        //для выдачи обработки отправки
        Label label = new Label();
        GridPane.setConstraints(label, 0,3);
        root.getChildren().addAll(label);
        //стили для выдачи обработки отправки
        label.setFont(Font.font("Sans", 14.0));
        label.setStyle("-fx-text-fill: darkslategrey");



        Scene scene = new Scene(root, 625,260);
        play.setOnAction(event -> {
            root.setStyle("-fx-background-color: antiquewhite");
            primaryStage.setScene(scene);
            primaryStage.show();
            new GameTimer(60);

        });

        clear.setOnAction(event -> {
            name.clear();
            label.setText(null);
        });




        scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
            public void handle(KeyEvent ke) {
                switch (ke.getCode().getName()){
                    //отправка сообщения принажатии на клавиатуре Enter
                    case "Enter": {
                        if (GameTimer.finish) {
                            int p = MonoThreadClientHandler.points;
                            //конечный экран со словами Вы ввели столько-то слов за 1 минуту
                            primaryStage.setScene(scene1);
                            primaryStage.show();
                        }
                        else {
                            if (!name.getText().isEmpty()){
                                try {
                                    // пишем сообщение клиента в канал сокета для сервера
                                    Client.out.writeUTF(name.getText());
                                    // проталкиваем сообщение из буфера сообщений в канал
                                    Client.out.flush();


                                    System.out.println("client wrote & start waiting for data from server...");

                                    // забираем ответ из канала сервера в сокете
                                    // клиента и сохраняем её в in переменную, печатаем на
                                    // консоль
                                    System.out.println("reading...");
                                    String inn = Client.in.readUTF();
                                    System.out.println(inn);
                                    //Thread.sleep(5000);
                                    serverTask.setText(Client.in.readUTF());
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                            else {
                                label.setText("Вы не ввели слово");
                            }
                        }

                    }
                }
            }
        });



    }

    public static void go() {
        launch();
    }
}
